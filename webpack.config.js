import path from 'path'
import { CleanWebpackPlugin } from 'clean-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import CopyWebpackPlugin from 'copy-webpack-plugin'
import CompressionPlugin from 'compression-webpack-plugin'

const __dirname = path.resolve()

export default {
  mode: 'production',
  target: ['web', 'es5'],
  entry:  {
    home_page: path.resolve(__dirname,'src/app.js'),
    project_details: path.resolve(__dirname,'src/projects.js')
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: '[name].js'
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CompressionPlugin(),
    new HtmlWebpackPlugin({
      filename: 'layout.ejs',
      template: '!!ejs-compiled-loader!/src/views/layout.ejs'
    }),
    new HtmlWebpackPlugin({
        filename: 'project-details.ejs',
        template: '!!ejs-compiled-loader!/src/views/project-details.ejs'
    }),
    new HtmlWebpackPlugin({
      filename: 'page_404.ejs',
      template: '!!ejs-compiled-loader!/src/views/page_404.ejs'
    }),
    new HtmlWebpackPlugin({
      filename: 'page_500.ejs',
      template: '!!ejs-compiled-loader!/src/views/page_500.ejs'
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: 'src/assets',
          to: 'assets'
        }
      ]
    })
  ],
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.ejs$/,
        loader: 'ejs-loader',
        options: {
          esModule: false
        }
      },
      {
          test: /\.(scss)$/,
          use: ['style-loader','css-loader','sass-loader']
      },
      {
        test: /\.(glsl|vs|fs|vert|frag)$/,
        exclude: /node_modules/,
        use: 'raw-loader',
      },
      {
				test: /\.(jpg|jpeg|png|gif|mp3|svg|ttf|woff2|woff|eot|webp)$/gi,
				use: {
					loader: "file-loader",
				}
			}
    ]
  }
};
