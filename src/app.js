import ScrollReveal from 'scrollreveal'
import './scss/style.scss'
import './skills.js'
import skills from './skills.json'
// import Sketch from './webGL.js'
// const sketch = new Sketch()

/** Scroll reveal */
const sr = ScrollReveal({
    distance: '60px',
    duration: 2500,
    delay: 400
})
sr.reveal(`.box-experience`, {origin: 'left', interval:100})
sr.reveal(`.box-expertise`, {origin: 'right', interval:100})
ScrollReveal().reveal(`#icon-hand-first`, {origin: 'top', delay:50, distance: '10px'})
ScrollReveal().reveal(`#icon-hand-second`, {origin: 'bottom', delay:50, distance: '10px'})
ScrollReveal().reveal('#sub-title-contact span', {distance: '0px', scale: 0, delay:0 });
ScrollReveal().reveal(`#datas-skills, #wrapper-carousel`, { scale: 0.75, distance: '0px', delay:600 });

/**Scroll sections active link */
const sections = document.querySelectorAll('section[id]')

const scrollActive = ()=>{
    const scrollY = window.pageYOffset
    sections.forEach(current =>{
        const sectionHeight = current.offsetHeight
        const sectionTop = current.offsetTop + 250
        const sectionId = current.getAttribute('id')

        if(scrollY > sectionTop && scrollY <= sectionTop + sectionHeight){
            document.querySelector('nav a[href*=' + sectionId + ']').classList.add('active-link')
        }else{
            document.querySelector('nav a[href*=' + sectionId + ']').classList.remove('active-link')
        }
    })
}
window.addEventListener('scroll', scrollActive)

/** Parallax */
const layer_0 = document.getElementById('layer_0')
const header = document.getElementById('main-title')
const layer_1 = document.getElementById('layer_1')
const layer_1_2 = document.getElementById('layer_1_2')
const layer_2 = document.getElementById('layer_2')
const layer_3 = document.getElementById('layer_3')
const layer_bg = document.getElementById('layer_bg')
const gradient_linear = document.getElementById('gradient-linear')
const second_bg_on_top = document.getElementById('second-bg-on-top')

const parallax = ()=>{
    const value = window.scrollY

    header.style.top = Math.min(10 +value * .25, second_bg_on_top.height) + 'vh'
    gradient_linear.style.top = Math.min(value * -.25, layer_0.height) +'px'
    layer_0.style.top = Math.min(value * -.25, layer_0.height) +'px'
    layer_1.style.top = Math.min(value * -.1, layer_1.height) +'px'
    // layer_1_2.style.top = Math.min(value * -.25, layer_1_2.height) +'px'
    layer_2.style.top = Math.min(value * -.25, layer_2.height) +'px'
    // layer_3.style.top = Math.min(value * -.25, layer_3.height) +'px'
    // layer_bg.style.top = Math.min(value * -.25, layer_bg.height) +'px'
    // second_bg_on_top.style.top = Math.min(90 + value * -.025, second_bg_on_top.height) +'vh'
}
window.addEventListener('scroll', parallax)

/** Toggle sidebar */
const buttonSidebar = document.querySelector('.burger')
const sidebar = document.getElementById('wrapper-sidebar')

buttonSidebar.addEventListener('click', () =>{
    buttonSidebar.classList.toggle('is-toggled')
    sidebar.classList.toggle('is-collapsed')
})

document.onclick = (event)=>{
    if(
        !sidebar.contains(event.target) && 
        event.target.id != 'span-menu-burger' && 
        event.target.id != 'menu-burger'
    ){
        sidebar.classList.remove('is-collapsed')
        buttonSidebar.classList.remove('is-toggled')
    } else if(
        event.target.id === 'presentation-url' ||
        event.target.id === 'exprertise-url' ||
        event.target.id === 'experience-url' ||
        event.target.id === 'contact-url'
    ){
        sidebar.classList.remove('is-collapsed')
        buttonSidebar.classList.remove('is-toggled')
    }
} 

/** Card experience */
const cardsExperience = document.querySelectorAll('.box-experience')
cardsExperience.forEach(item => { 
    item.addEventListener('click', ()=>{
        item.classList.toggle('is-active')
    })
})

/** Contact */
const inputs = document.querySelectorAll('input')
const textarea = document.querySelector('textarea')
inputs.forEach(input => { 
    input.addEventListener('change', ()=>{
        const label = document.querySelector(`label[for=${input.id}]`)
        if(input.value.length === 0){
            label.style.top = '10px'
            label.style.fontSize = '1rem'
        }
        if(input.value.length > 0){
            label.style.top = '-5px'
            label.style.fontSize = '.75rem'
        }
    })
})
textarea.addEventListener('change', ()=>{
    const label = document.querySelector(`label[for=${textarea.id}]`)
        if(textarea.value.length === 0){
            label.style.top = '10px'
            label.style.fontSize = '1rem'
        }
        if(textarea.value.length > 0){
            label.style.top = '-5px'
            label.style.fontSize = '.75rem'
        }
})

/** Carousel */
const content_carousel = document.querySelector('.content-carousel')
const index_skills = document.querySelector('#index-skills')
const length_skills = document.querySelector('#length-skills')
const middleCountLabels = 20

const arraySkills = []
const createArraySkills = ()=>{
    for (let index = 0; index < skills.children.length; index++) {
        for (let skill = 0; skill < skills.children[index].children.length; skill++) {
            arraySkills.push({name: skills.children[index].children[skill].name, value: skills.children[index].children[skill].value})
        } 
    }
}
createArraySkills()

let currentSkillChecked = middleCountLabels

const displayCountSkills = (data)=>{
    currentSkillChecked = data
    index_skills.textContent = currentSkillChecked
}
displayCountSkills(currentSkillChecked)

const checkMyLevel = (data)=>{
    let levelOfCurrent
    if( data > 0 && data <= 15) levelOfCurrent =  'foundation'
    if( data > 15 && data <= 40) levelOfCurrent = 'intermediate'
    if( data > 40 && data <= 100) levelOfCurrent = 'advanced'
    return levelOfCurrent
}

const arrayInputs = []
const addInputs = ()=>{

    for (let index = 0; index < arraySkills.length; index++) {
        const input = document.createElement('input')
        arrayInputs.push(input)
    }

    const toAdd = document.createDocumentFragment()
    for (let index = 0; index < arrayInputs.length; index++) {
        let id_input = 's' + index

        arrayInputs[index].setAttribute('id', id_input)
        arrayInputs[index].setAttribute('class', id_input)
        arrayInputs[index].setAttribute('name', 'slider')
        arrayInputs[index].setAttribute('type', 'radio') 
        index === middleCountLabels ? arrayInputs[index].checked = true : false
        toAdd.appendChild(arrayInputs[index])
    }
        
    content_carousel.appendChild(toAdd)
}

const addLabels = ()=>{
    const toAdd = document.createDocumentFragment()

    const arrayLabelsBG = []
    for (let index = 0; index < arraySkills.length; index++) {
        const labelBG = document.createElement('label')
        arrayLabelsBG.push(labelBG)
    }
    for (let index = 0; index < arrayLabelsBG.length; index++) {
        let id_input = 's' + index
        let class_label= 'slide'+ index

        arrayLabelsBG[index].innerHTML = `<p>${arraySkills[index].name}</p>`
        arrayLabelsBG[index].setAttribute('for', id_input)
        arrayLabelsBG[index].setAttribute('class', class_label)
        arrayLabelsBG[index].classList.add('bg')
        toAdd.appendChild(arrayLabelsBG[index])
    }
    content_carousel.appendChild(toAdd)
    
    const arrayLabels = []
    for (let index = 0; index < arraySkills.length; index++) {
        const label = document.createElement('label')
        arrayLabels.push(label)
    }
    for (let index = 0; index < arrayLabels.length; index++) {
        let id_input = 's' + index
        let class_label= 'slide'+ index
        let levelOfCurrent = checkMyLevel(arraySkills[index].value)

        arrayLabels[index].innerHTML = `<p>${levelOfCurrent}</p>`
        arrayLabels[index].setAttribute('for', id_input)
        arrayLabels[index].setAttribute('class', class_label)
        arrayLabels[index].classList.add('data')
        arrayLabels[index].classList.add(levelOfCurrent)
        toAdd.appendChild(arrayLabels[index])
    }
    content_carousel.appendChild(toAdd)
}

const initCarousel = ()=>{
    length_skills.textContent = arraySkills.length
    addInputs()
    addLabels()
}
initCarousel()

// arrows and count skills
const updateCountSkillsWithArrows = (directionCarousel)=>{
    console.log(directionCarousel)
    if(directionCarousel === 'left' && currentSkillChecked >= 1){
        console.log(currentSkillChecked)
        --currentSkillChecked
        currentSkillChecked===0?displayCountSkills(currentSkillChecked+1):displayCountSkills(currentSkillChecked)
        document.querySelector(`#s${currentSkillChecked}`).checked = true
    }else if(directionCarousel === 'right' && currentSkillChecked < arraySkills.length){
        ++currentSkillChecked
        displayCountSkills(currentSkillChecked)
        document.querySelector(`#s${currentSkillChecked}`).checked = true
    }
}

arrayInputs.forEach(input => { 
    input.addEventListener('change', ()=>{
        displayCountSkills(input.className.slice(1, 3))
    })
})

const changeCheckedSkill = ()=>{
    const buttonLeftArrow = document.getElementById('leftArrow')
    const buttonRightArrow = document.getElementById('rightArrow')

    buttonLeftArrow.addEventListener('click', () =>{
        updateCountSkillsWithArrows ('left')
    })

    buttonRightArrow.addEventListener('click', () =>{
        updateCountSkillsWithArrows ('right')
    })
} 
changeCheckedSkill()