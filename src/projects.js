const scroll_down = document.getElementById("scroll-down")

const myScrollFunc = function () {
    const y = window.scrollY
    if (y >= window.screen.height - 400){
        scroll_down.classList.add('is-hidden')
    } else if(y === 0){
        scroll_down.classList.remove('is-hidden')
    }
}

window.addEventListener("scroll", myScrollFunc)