import dotenv from 'dotenv'
import express from 'express'
import compression from 'compression'
import path from 'path'
import { readFile } from 'fs/promises'
const json = JSON.parse(await readFile(new URL('./src/projects.json', import.meta.url)))

dotenv.config()
const __dirname = path.resolve()
const app = express()
const PORT = process.env.PORT || 5000

let setCache = function (req, res, next) {
    const period = 60 * 5 
    if (req.method == 'GET') {
      res.set('Cache-control', `public, max-age=${period}`)
    } else {
      res.set('Cache-control', `no-store`)
    }
    next()
}
  
app.use(express.static(path.join(__dirname, 'public')))
app.use(setCache)
app.use(compression())

app.set('views', './public')
app.set('view engine', 'ejs')

app
    .get('/', (req, res)=>{
        res.render('layout', {projects : json, class_footer: 'home'})
    })
    .get('/newtspell', (req, res)=>{
        res.render('project-details', {project : json.project_1, class_footer: 'project'})
    })
    .get('/clone_mixamo', (req, res)=>{
        res.render('project-details', {project : json.project_2, class_footer: 'project'})
    })
    .get('/dev_ops', (req, res)=>{
        res.render('project-details', {project : json.project_3, class_footer: 'project'})
    })
    .get('*', (req, res)=>{
        res.status(404).render('page_404')
    })
    .get('*', (req, res)=>{
        res.status(500).render('page_500')
    })
    .listen(PORT, () => console.log(`> http://localhost:${PORT}`))